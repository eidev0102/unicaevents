(function($) {

  $('.card-slider').slick({
    dots: false,
    arrows: true,
    prevArrow: '<i class="icon-left-arrow icon-bg"></i>',
    nextArrow: '<i class="icon-right-arrow icon-bg"></i>',
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 481,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }]
  });

  $('.banner-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    cssEase: 'linear',
    arrows: true,
    prevArrow: '<i class="icon-left-arrow icon-bg"></i>',
    nextArrow: '<i class="icon-right-arrow icon-bg"></i>',
    responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
      }
    }]
  });

      // Add smooth scrolling to all links
      $("#return-to-top, #myNavbar .nav li a").on('click', function(event) {

                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                          scrollTop: $(hash).offset().top
                        }, 800, function() {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                          });
                    } // End if
                  });

      $(window).scroll(function() {
        if ($(this).scrollTop() >= 100) {  
          $("#return-to-top").addClass("show");     
        } else {
          $('#return-to-top').removeClass('show');
        }
      });

      $('#return-to-top').click(function() {      
        $('body,html').animate({
          scrollTop : 0                    
        }, 500);
        ('#return-to-top').removeClass('show');
      });

      $('#nav-icon1').on('click', function(){
        $(this).toggleClass('open');
      });

      // parrelx effect
      $(document).ready(function() {
        $('.foreground').mouseParallax({ moveFactor: 3 });
      });


    // counting section js
    var x = '';

    $(function(){
      x = $(".counting").offset();
    });

    $(window).on('scroll', function() {
      scrollPosition = $(this).scrollTop();
            // alert(x.top + "px")
            if (scrollPosition >= x.top - 300) {
              $(".num").numScroll();
        // If the function is only supposed to fire once
        $(this).off('scroll');
      }
    });


    // add class for mac 
    

  })(jQuery);